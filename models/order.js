const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userID:{
			type: String,
			required: [true, 'User ID is required']
		},

	products: [{
			productId:{
				type: String,
				require: [true, 'Product Id is required']
			},
			quantity:{
				type: Number,
				require: [true, 'quantity is required']
			}
		}],

	totalAmount:{
			type: Number,
			require: [true, 'Total amount is required']
		},
	purchasedOn:{
			type: Date,
			default: new Date()
		}
})


module.exports = mongoose.model ("Order", orderSchema)