const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
	firstName: {
		type:String,
		require: [true, "First name is required"]
	},
	lastName: {
		type:String,
		require: [true, "Last name is required"]
	},
	email : {
		type :String,
		require : [ true, 'Email address is required']
	},
	password : {
		type : String,
		require : [true, 'Password is required']
	},
	isAdmin : {
		type: Boolean,
		default : false
	},
	order : [{
		products: [{
			productId:{
				type: String,
				require: [true, 'Product Id is required']
			},
			quantity:{
				type: Number,
				require: [true, 'quantity is required']
			},
		}],
	totalAmount:{
			type: Number,
			require: [true, 'Total amount is required']
		},
	purchasedOn:{
			type: Date,
			default: new Date()
		}
	}]
})



module.exports = mongoose.model ("User", userSchema)