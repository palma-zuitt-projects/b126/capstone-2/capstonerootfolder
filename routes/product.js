const express = require ('express');
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");


//route in creating new product
router.post("/", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send({auth: "failed"})
	}
})

//route for getting all active products

router.get("/active", (req,res) => {
	console.log(req)
	productController.getActiveProducts().then(resultFromController => res.send (
		resultFromController))
})

//route for getting all products
router.get("/", (req,res) => {
	console.log(req)
	productController.getAllProducts().then(resultFromController => res.send (
		resultFromController))
})

//route for getting a specific product
router.get("/:productId", (req, res) => {
	productController.getSpecificProduct(req.params).then(resultFromController => res.send (
		resultFromController))
})


//route for updating a specific product
router.put("/:productId", auth.verify, (req, res) =>{
	if(auth.decode(req.headers.authorization).isAdmin) {
		productController.updateSpecificProduct(req.params, req.body).then(resultFromController => res.send (
		resultFromController))
	}else{
		res.send({auth: "failed"})
	}
})

//route for archive

router.delete("/:productId", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin) {
		productController.archiveSpecificProduct(req.params).then(resultFromController => res.send (
		resultFromController))
	}else{
		res.send({auth: "failed"})
	}

})


module.exports = router;
