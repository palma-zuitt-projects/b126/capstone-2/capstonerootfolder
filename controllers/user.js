//import dependencies
const User = require("../models/user");
const Product = require ("../models/product");
const Order =require ("../models/order");

const bcrypt = require("bcrypt");//is used to encrypt our passwords
const auth = require("../auth");//import auth.js file to use its authorization functions


//register new user
module.exports.registerUser = (body) => {
	let newUser = new User ({
		firstName : body.firstName,
		lastName : body.lastName,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10)
	})

	return newUser.save().then((user,error) => {
		if(error){
			return false; // user was NOT saved
		}else{
			return true; // user was successfully saved
		}
	})
}

//check for duplicate emails
module.exports.checkEmail = (body) => {
	return User.find({email: body.email}).then(result => {
		if(result.length > 0){
			return true; // duplicate email found
		}else{
			return false; //email not yet registered
		}
	})	
}


//login user
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false // user does not exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {accessToken: auth.createAccessToken(result.toObject())}
			}else{
				return false; //meaning passwords do not match
			}
		}
	})
}

/*module.exports.loginWithOrders = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false // user does not exist
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return User.findOne({email: body.email}).then(result => {
					firstName: body.firstName
				})

			}else{
				return false; //meaning passwords do not match
			}
		}
	})
}*/